var app = app || {};

$(function () {

    new app.LetterCollectionView();
    new app.LetterRouter();

    /*backbone tooltip*/
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });

    /*costom content scroller*/
    $(document).on("load",function(){
        $(".content").mCustomScrollbar();
    });

});


var app = app || {};

app.LetterModel = Backbone.Model.extend({

    url: 'letters.json'

    // default: {
    //     name: "",
    //     mail: "",
    //     topic: "",
    //     message: "",
    //     dataCreated: "",
    //     letterType: "", // spam, delete, sent, inbox, drafts
    //     letterRead: true, // false
    //     inChecked: false, // true
    //     inFavorite: false, // true
    //     inImportant: false, // true
    //     inAttached: false // true
    // }

});

var app = app || {};

app.LetterCollection = Backbone.Collection.extend({

    model: app.LetterModel,

    url: 'letters.json',

    initialize: function () {
        this.fetch({
            reset: true
        });
    }

});

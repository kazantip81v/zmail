var app = app || [];

app.LetterModelView = Backbone.View.extend({

    tagName: 'tr',
    className: 'unread',

    template: _.template($('#tplLetter').html()),
    templateOpen: _.template($('#openLetter').html()),

    events: {
        'change [name=checked]': 'letterSelect',
        'click td > div': 'letterOpen',
        'click td[data-title="star"]': 'letterFavorite',
        'click td[data-title="important"]': 'letterImportant',
        'click .button-filter': 'filter'

    },

    initialize: function () {
        this.listenTo(app.model, 'all', this.render);
    },

    render: function () {
        this.$el.html( this.template( this.model.toJSON() ) );
        return this;
    },
    renderOpen: function () {
        this.$el.html( this.templateOpen (this.model.toJSON() ) );
        return this;
    },

    markColor: function () {
        var markColor = $('input[name="color"]:checked');
        if (markColor) {
            markColor.siblings().addClass('fa-color');
        }
    },

    letterOpen: function () {
        $('#back, .spam, .delete, .move').removeClass('display-none'); //hide
        $('.select, .refresh').addClass('display-none');
        this.model.save({letterRead: true, inChecked: true});

        var openLetter =  new app.LetterModelView({
            model: this.model
        });
        $('tbody').empty().append(openLetter.renderOpen().el).children().attr({'id':'letterOpen'});
        this.markColor();
    },

    letterSelect: function () {
        var checked = $('[name=checked]:checked');
        this.$el.toggleClass('letter-checked');
        this.$('td:first').toggleClass('visibility-hidden');

        var inChecked = this.model.get('inChecked');
        if (!inChecked) {
            this.model.save({inChecked: true});
            if (checked.length) {
                $('.spam, .delete, .move').removeClass('display-none');
                $('.refresh').addClass('display-none');
            }
        } else {
            this.model.save({inChecked: false});
            if (!checked.length) {
                $('.spam, .delete, .move').addClass('display-none'); //hide
                $('.refresh').removeClass('display-none');
            }
        }
    },

    letterFavorite: function () {
        this.$el.find('td[data-title="star"] .fa').toggleClass('fa-color');
        this.model.save({inFavorite: true});
    },

    letterImportant: function () {
        this.$el.find('td[data-title="important"] .fa').toggleClass('fa-color');
        this.model.save({inImportant: true});
    },

    filterType: function () {
        var valueButton = $('button.active').val(),
            typeLetter = this.model.get('letterType');
        if (valueButton === 'starred') typeLetter = valueButton;
        if (valueButton === 'important') typeLetter = valueButton;
        if (valueButton === 'allMail') typeLetter = valueButton;
        if (valueButton === typeLetter) {
            return true;
        } else return false;
    }

});

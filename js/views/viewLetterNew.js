var app = app || {};

app.LetterNewView = Backbone.View.extend({

    id: 'letterNew',

    template: _.template($('#newLetter').html()),

    events: {

    },

    initialize: function () {
        this.listenTo(this.modelNew, 'change', this.render);
    },

    render: function () {
        this.$el.html( this.template() );
        return this;
    }
});
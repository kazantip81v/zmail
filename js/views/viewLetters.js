var app = app || {};

app.LetterCollectionView = Backbone.View.extend({

    // el: '#content',
    el: '#container',

    events: {
        'click .refresh': 'refresh',
        'click #back' : 'back',
        'click  a[href="#new-mails"]': 'letterNew',
        'click #cancel': 'letterNewCancel',
        'click .send': 'letterNewSend',
        'click .save': 'letterNewSave',
        'click .button-cc': 'letterSendCopy',
        'click .button-filter': 'letterFilterTarget',
        'click input[type="radio"]': 'select',
        'click [data-original-title="Spam"]': 'moveToSpam',
        'click [data-original-title="Delete"]': 'moveToTrash',
        'click .button-add-file': 'clickInputFail'
    },

    initialize: function () {
        this.collection = new app.LetterCollection();

        this.listenTo(this.collection, 'reset', this.render);
    },

    renderEach: function (model) {
        var view = new app.LetterModelView({
            model: model
        });

        view.render();
        var filterType = view.filterType();
        if (filterType) {
            this.$tbody.append(view.el);
            this.markColor();
            this.markRead();
        }
    },

    render: function () {
        this.$tbody = this.$('tbody');
        this.collection.each(this.renderEach, this);
    },

    markColor: function () {
        var markColor = $('[name=color]:checked');
        markColor.each(function (index, element) {
            if (element.hasAttribute('checked')) {
                $(element).siblings().addClass('fa-color');
            }
        });
    },

    markRead: function () {
        var markRead = $('.read:checked');
        markRead.each(function (index, element) {
            if (element.hasAttribute('checked')) {
                $(element).next().removeClass('unread').parent().siblings().find('.unread').removeClass('unread');
            }
        });
    },
    back: function () {
        this.$('#back').addClass('display-none');
        this.$('.select').removeClass('display-none');
        this.$('tbody').empty();
        this.render();
    },

    refresh: function () {
        //location.reload();
        this.$('tbody').empty();
        this.render();
    },

    letterNew: function () {
        var newLetter = new app.LetterNewView();
        $('tbody').empty().append(newLetter.render().el);
        $('.send, .save, #cancel').removeClass('display-none'); //hide
        $('.select, .refresh, .more, .move, .delete, .spam, #back').addClass('display-none');
    },

    letterNewCancel: function () {
        this.$('#letterNew').remove();
        this.render();
        $('.send, .save, #cancel').toggleClass('display-none');
        $('.select, .refresh, .more').toggleClass('display-none');
    },

    letterNewSend: function (event) {
        var $form = this.$('.send-form'),
            //fd = new FormData(),
            dataObj = {
                sendMail: $form.find('#email').val(),
                topic: $form.find('#topic').val(),
                message: $form.find('#message').val(),
                dataCreated: Date.now(),
                letterType: 'drafts'
            };

       /* fd.append('sendMail', $form.find('#email').val());
        fd.append('topic', $form.find('#subject').val());
        fd.append('content', $form.find('#comment').val());
        fd.append('file', $form.find('#file').val());
        fd.append('dataCreated', Date.now());
        fd.append('letterType', 'send');*/
        //this.collection.create(dataObj);

        this.collection.add(dataObj);
        event.preventDefault();

    },

    letterNewSave: function () {
        var $form = this.$('.send-form'),
            //fd = new FormData(),
            dataObj = {
                sendMail: $form.find('#email').val(),
                topic: $form.find('#topic').val(),
                message: $form.find('#message').val(),
                dataCreated: Date.now(),
                letterType: 'sent'
            };
    },

    letterSendCopy: function () {
        var $buttonVal = event.target.value;
        if ($buttonVal === 'cc') $('.cc').toggleClass('hidden');
        if ($buttonVal === 'bcc') $('.bcc').toggleClass('hidden');
    },

    letterFilterTarget: function (event) {
        var $text = event.target.value;

        $('.active').removeClass('active');
        $('[value = "' + $text + '"]').addClass('active').siblings().addClass('active');

        this.$('tbody').empty();

        if ($text === 'allMail') {
            this.render();
        }
        if ($text === 'starred')
        {
            var collectionFilteredFavorite = this.collection.where({inFavorite: true});
            collectionFilteredFavorite.forEach(this.renderEach, this);
        }
        if ($text === 'important') {
            var collectionFilteredImportant = this.collection.where({inImportant: true});
            collectionFilteredImportant.forEach(this.renderEach, this);
        }
        else {
            var collectionFilteredType = this.collection.where({letterType: $text});
            collectionFilteredType.forEach(this.renderEach, this);
        }
    },


    select: function (event) {
        var coll,
            text = $('button.button-filter.active').val(),
            target = event.target.value,
            elem = $('[name=checked]'),
            elemStar = $('.star'),
            elemRead = $('.read');

        function resetElem (elem) {
            elem.prop('checked', false);
            $('tr').removeClass('letter-checked').find('td:first').addClass('visibility-hidden');
        }

        function shangeStyle () {
            $('[name=checked]:checked').parent().prev().removeClass('visibility-hidden')
                .parent().addClass('letter-checked');
        }
        function showButton () {
            $('.spam, .delete, .move').removeClass('display-none');
            $('.refresh').addClass('display-none');
        }

        /*this.$('#table tbody').find('tr').filter(function (index, elem) {
            return !_.isEmpty($(elem).has('.unread'));
        })
            .find('.' + target + ':checked')
            .closest('tr')
            .addClass('letter-checked')
            .find('td:first')
            .removeClass('visibility-hidden');*/

        switch (target){
            case 'all':
                elem.prop('checked', true);
                this.$('tr').addClass('letter-checked').find('td:first').removeClass('visibility-hidden');
                showButton();
                coll = this.collection.where({letterType: text});
                coll.forEach(function (model) {
                    model.save({inChecked: true});
                });
                break;
            case 'none':
                resetElem(elem);
                this.$('.spam, .delete, .move').addClass('display-none');
                this.$('.refresh').removeClass('display-none');
                coll = this.collection.where({letterType: text});
                coll.forEach(function (model) {
                    if (model.get('inChecked')) {
                        model.save({inChecked: false});
                    }
                });
                break;
            case 'read':
                resetElem(elem);
                this.$('.read:checked').parent().siblings().find('[name=checked]').prop('checked', true);
                shangeStyle();
                showButton();
                coll = this.collection.where({letterType: text});
                coll.forEach(function (model) {
                    if (model.get('letterRead')) {
                        model.save({inChecked: true});
                    } else { if (model.get('inChecked')) model.save({inChecked: false});
                    }
                });
                break;
            case 'unread':
                resetElem(elem);
                elemRead.each(function (index, el) {
                    if (!el.hasAttribute('checked')) {
                        $(el).parent().siblings().find('[name=checked]').prop('checked', true);
                    }
                });
                shangeStyle();
                showButton();
                coll = this.collection.where({letterType: text});
                coll.forEach(function (model) {
                    if (!model.get('letterRead')) {
                        model.save({inChecked: true});
                    } else { if (model.get('inChecked')) model.save({inChecked: false});
                    }
                });
                break;
            case 'starred':
                resetElem(elem);
                this.$('.star:checked').parent().prev().find('[name=checked]').prop('checked', true);
                shangeStyle();
                showButton();
                coll = this.collection.where({letterType: text});
                coll.forEach(function (model) {
                    if (model.get('inFavorite')) {
                        model.save({inChecked: true});
                    } else { if (model.get('inChecked')) model.save({inChecked: false});
                    }
                });
                break;
            case 'unstarred':
                resetElem(elem);
                elemStar.each(function (index, el) {
                    if (!el.hasAttribute('checked')) {
                        $(el).parent().prev().find('[name=checked]').prop('checked', true);
                    }
                });
                shangeStyle();
                showButton();
                coll = this.collection.where({letterType: text});
                coll.forEach(function (model) {
                    if (!model.get('inFavorite')) {
                        model.save({inChecked: true});
                    } else { if (model.get('inChecked')) model.save({inChecked: false});
                    }
                });
                break;
        }
    },

    moveToSpam: function () {
        var coll = this.collection.where({inChecked: true});
        coll.forEach( function (model) {
            model.save({letterType: 'spam', inChecked: false});
        }, this);
        this.$tbody.empty();
        this.render();
        this.$('.spam, .delete, .move, #back').addClass('display-none');
        this.$('.select, .refresh').toggleClass('display-none');
    },

    moveToTrash: function () {
        var coll = this.collection.where({inChecked: true});
        coll.forEach( function (model) {
            model.save({letterType: 'delete', inChecked: false});
        }, this);
        this.$tbody.empty();
        this.render();
        this.$('.spam, .delete, .move, #back').addClass('display-none');
        this.$('.select, .refresh').toggleClass('display-none');
    }

    // clickInputFail: function () {
    //     setTimeout(function () {
    //         $('#file').click();
    //     }, 0);
    // }

});

var app = app || {};

app.LetterRouter = Backbone.Router.extend({

    routes: {
        'en/email' : 'mailBox',
        'en/calendar' : 'calendarBox',
        'en/contacts' : 'contactsBox',
        'en/todos' : 'todosBox'
    },

    initialize: function () {
        Backbone.history.start();
    },

    mailBox: function () {
        console.log('Mail Box !!!');
        $('.vor').hide();
        $('#hiro1').show();
    },

    calendarBox: function () {
        console.log('Calendar !!!');
        $('.vor').hide();
        $('#hiro2').show().removeClass('hide');
    },
    contactsBox: function () {
        console.log('Contacts !!!');
        $('.vor').hide();
        $('#hiro3').show().removeClass('hide');
    },
    todosBox: function () {
        console.log('Todos !!!');
        $('.vor').hide();
        $('#hiro4').show().removeClass('hide');
    }
});


